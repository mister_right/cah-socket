const { Pool } = require('pg')

const connectionString = process.env.PSQL_CONN_STRING
console.log(connectionString)
const pool = new Pool({ connectionString })

module.exports = {
    query: (text, params, callback) => {
        return pool.query(text, params, callback)
    },
    getGameData: async (gameUuid) => {
        return await pool.query('select * from games where game_uuid = $1', [gameUuid])
    },
    startGame: async (gameUuid) => {
        await pool.query('update games set started = true where game_uuid = $1' , [gameUuid])
    },
    endGame: async (gameUuid, winner, winnerId) => {
        await pool.query('update games set ended = true, winner_name = $2, winner_id = $3 where game_uuid = $1' , [gameUuid, winner, winnerId])
    },
    getBlackCard: async () => {
        return await pool.query('select text, fields from cards where black is true order by random() limit 1')
    },
    getWhiteCards: async (deckId, n) => {
        return await pool.query(`select * from cards where deck_id = $1 and white is true order by random() limit ${n}`, [deckId])
    }
}