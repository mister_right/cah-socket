const express = require('express')
const http = require('http')
const jwt = require('jsonwebtoken')
const { Server } = require("socket.io");
const { Room } = require('./room')

const jwtKey = process.env.JWT_KEY
console.log(jwtKey)
const db = require('./db')

const app = express();
const httpServer = http.createServer(app);
const io = new Server(httpServer, { cors: { origin: '*' } });

const rooms = {}
const ns = io.of("/");

ns.on('connection', (socket) => {
    console.log('connection')

    socket.on('join', async (roomId, token, callback) => {
        const decoded = jwt.verify(token, jwtKey)
        const data = await db.getGameData(roomId)
        const game = data.rows[0]

        if (game.game_uuid.toString() !== decoded.game_uuid.toString()) {
            ns.to(socket.id).emit('error', 'Invalid token.')
            return
        }
        if (!Object.keys(rooms).includes(roomId)) {
            const room = new Room(socket.id, game.game_name, game.deck_id, game.max_score)
            rooms[roomId] = room
        }
        else if (rooms[roomId].userExists(decoded.username) === true) {
            ns.to(socket.id).emit('message', { message: `This name is already taken, choose another one.` })
            return
        }

        rooms[roomId].addUser(socket.id, decoded)
        socket.join(roomId)
        ns.to(roomId).emit('users', rooms[roomId].getUsers())
        ns.to(roomId).emit('message', { message: `${decoded.username} joins the game.` })
        callback({ data: rooms[roomId].getData() })
    })

    socket.on('start', async (roomId) => {
        const room = rooms[roomId]
        if (room.owner !== socket.id) {
            ns.to(roomId).emit('message', { message: `${socket.id} is impatient.` })
            return
        }
        if (room.getNumberOfUsersOnline() < 3) {
            ns.to(socket.id).emit('message', { message: `Not enough users to start game, need at least 3.` })
            return
        }

        room.startGame()
        await db.startGame(roomId)

        const black = await db.getBlackCard()
        room.setBlack(black.rows[0])

        for (let username in room.users) {
            const whiteCards = await db.getWhiteCards(room.deckId, 12)
            room.setUserWhiteCards(username, whiteCards.rows)
            const userSocketId = room.getUserSocketId(username)
            ns.to(userSocketId).emit('cards', whiteCards.rows)
        }

        ns.to(roomId).emit('users', room.getUsers())
        ns.to(roomId).emit('data', room.getData())
        ns.to(roomId).emit('message', { message: `Game started, ${room.reader} is reading now.` })
    })

    socket.on('chosenCards', (roomId, cards) => {
        const room = rooms[roomId]
        room.setUserChosenCards(socket.id, cards)
        const [usersCards, roundEnd] = room.getUsersCards()
        if (roundEnd === false) {
            ns.to(roomId).emit('users', room.getUsers())
            return
        }
        const data = room.getData()
        data['usersCards'] = usersCards
        ns.to(roomId).emit('data', data)
        if (room.isReaderOnline() === true) return
        ns.to(roomId).emit('message', { message: `${room.reader} is offline, everyone can choose a winner now.` })
    })

    socket.on('roundEnd', async (roomId, winnerName) => {
        const room = rooms[roomId]
        const username = room.getUserName(socket.id)
        if (room.users[room.reader].online === false) {
            if (username === winnerName){
                ns.to(socket.id).emit('message', { message: `You can't choose yourself as a winner.` })
                return
            }
        }
        else if (username != room.reader) {
            ns.to(socket.id).emit('message', { message: `You are not reading now.` })
            return
        }

        room.endRound(winnerName)
        const winnerOfTheGame = room.isGameEnded()
        if (winnerOfTheGame !== null) {
            const userId = room.getUserId(winnerOfTheGame)
            await db.endGame(roomId, winnerOfTheGame, userId)
            ns.to(roomId).emit('message', { message: `Game ended, ${winnerOfTheGame} won.` })
            ns.to(roomId).emit('gameEnd', winnerOfTheGame)
            return
        }

        const black = await db.getBlackCard()
        room.setBlack(black.rows[0])

        for (let username in room.users) {
            const n = room.getNumberOfMissingCards(username)
            const white = await db.getWhiteCards(room.deckId, n)
            room.addUserWhiteCards(username, white.rows)
            const userSocketId = room.getUserSocketId(username)
            ns.to(userSocketId).emit('cards', room.getUserWhiteCards(username))
        }

        ns.to(roomId).emit('data', room.getData())
        ns.to(roomId).emit('users', room.getUsers())
        ns.to(roomId).emit('sound', room.getUserSound(winnerName))

        ns.to(roomId).emit('message', { message: `${winnerName} won round.` })
        ns.to(roomId).emit('message', { message: `${room.reader} is reading now.` })
    })

    socket.on('disconnecting', () => {
        console.log('disconnecting', socket.id)
        socket.rooms.forEach(i => {
            if (i === socket.id) return
            const leftistName = rooms[i].getUserName(socket.id)
            rooms[i].setUserOffline(socket.id)
            if (rooms[i].owner === null) {
                delete rooms[i]
                return
            }
            if (rooms[i].getNumberOfUsersOnline() < 3) {
                ns.to(i).emit('message', { message: `Warning! Not enaugh online users to play the game.` })
            }
            ns.to(i).emit('message', { message: `${leftistName} left the game.` })
            ns.to(i).emit('users', rooms[i].getUsers())
        })
    })
})

httpServer.listen(3000, () => console.log('listening on port 3000'));
